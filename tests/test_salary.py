from fastapi.testclient import TestClient
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from sql_app import models
from main import app
from sql_app import crud


SQLALCHEMY_TEST_DATABASE_URL = "sqlite:///./app.db"

engine = create_engine(SQLALCHEMY_TEST_DATABASE_URL, connect_args={"check_same_thread": False})

TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

client = TestClient(app)

def setup_function():
    models.Base.metadata.drop_all(bind=engine)
    models.Base.metadata.create_all(bind=engine)


# The user has registered, but the salary has not been set for him
def test_null_salary():
    setup_function()
    
    response = client.post(
        "/auth/register",
        json={"username": "testuser", "password": "testpassword"},
    )

    response = client.post(
        "/auth/login",
        json={"username": "testuser", "password": "testpassword"},
    )
    token = response.json()["token"]    

    response = client.get(
        "/salary",
        params={"token": token}
    )

    assert response.status_code == 200
    assert response.json() == {"detail": "Salary info not found or not set"}


# The user has registered, but the salary has not been set for him
def test_salary():
    setup_function()
    
    username = "testuser"
    password = "testpassword"
    salary = 25000
    promotion_date = datetime(year=2024, month=11, day=27)
    
    response = client.post(
        "/auth/register",
        json={"username": username, "password": password},
    )
    
    with TestingSessionLocal() as session:
        db_user = crud.get_user_by_username(session, username)
        crud.create_salary(session, db_user.id, salary, promotion_date)  

    response = client.post(
        "/auth/login",
        json={"username": username, "password": password},
    )
    token = response.json()["token"]
    response = client.get(
        "/salary",
        params={"token": token}
    )
    assert response.status_code == 200
    assert response.json() == {"salary": salary, "promotion_date": promotion_date.strftime("%Y-%m-%d %H:%M:%S")}
