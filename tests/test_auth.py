from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from sql_app import models
from main import app


SQLALCHEMY_TEST_DATABASE_URL = "sqlite:///./app.db"

engine = create_engine(SQLALCHEMY_TEST_DATABASE_URL, connect_args={"check_same_thread": False})

TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

client = TestClient(app)

def setup_function():
    models.Base.metadata.drop_all(bind=engine)
    models.Base.metadata.create_all(bind=engine)


def test_register():
    setup_function()

    response = client.post(
        "/auth/register",
        json={"username": "testuser", "password": "testpassword"},
    )

    assert response.status_code == 200
    assert response.json() == {"detail": "Successfully registered. Login to get token."}


def test_reply_register():
    setup_function()

    response = client.post(
        "/auth/register",
        json={"username": "testuser", "password": "testpassword"},
    )
    
    response = client.post(
        "/auth/register",
        json={"username": "testuser", "password": "testpassword"},
    )

    assert response.status_code == 400
    assert response.json() == {"detail": "User with this username already exists"}


def test_register_simple_password():
    setup_function()

    response = client.post(
        "/auth/register",
        json={"username": "testuser", "password": "qwerty"},
    )
    
    assert response.status_code == 400
    assert response.json() == {"detail": "Password should be at least 8 characters"}


def test_register_simple_username():
    setup_function()

    response = client.post(
        "/auth/register",
        json={"username": "test", "password": "testtest123TEST"},
    )

    assert response.status_code == 400
    assert response.json() == {"detail": "Username should be at least 8 characters"}


def test_login():
    setup_function()
    
    response = client.post(
        "/auth/register",
        json={"username": "testuser", "password": "testpassword"},
    )

    assert response.status_code == 200

    response = client.post(
        "/auth/login",
        json={"username": "testuser", "password": "testpassword"},
    )

    assert response.status_code == 200
    assert "token" in response.json()
    assert "expiry" in response.json()
