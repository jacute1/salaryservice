# Salary Service REST API Documentation

## Overview
This API provides endpoints to view current salary and the date of the next promotion for each employee. Each employee can only access their own salary information.

## How to run
- `git clone https://gitlab.com/jacute1/salaryservice.git`
- If you want to use postgres, specify its parameters and set DEBUG=False.
- If you want to use sqlite3, set DEBUG=True
### Docker
```
docker compose up --build
```
In the docker container, nginx waits for requests on port 80 and proxies them to port 8000
### Usual run
```
pip3 install -r requirements.txt
python3 app/main.py
```
### Tests
```
pip3 install -r requirements.txt
PYTHONPATH=./app/ DEBUG=True pytest
```
## Authentication

To ensure security, the API implements a method to generate a random secret token. This token is required to access salary information. Implemented validators for checking username and password.

### Register User [/auth/register]

#### Register User [POST]
Registers a new user.

+ Request (application/json)
    + Body
        ```
        {
            "username": "string",
            "password": "string"
        }
        ```

+ Response 200 (application/json)
    + Body
        ```
        {
            "detail": "Successfully registered. Login to get token."
        }
        ```

#### Login [/auth/login]

#### Login User [POST]
Logs in an existing user and returns a token.

+ Request (application/json)
    + Body
        ```
        {
            "username": "string",
            "password": "string"
        }
        ```

+ Response 200 (application/json)
    + Body
        ```
        {
            "token": "string",
            "expiry": "datetime"
        }
        ```

## Salary

Salary and promotion date are not set automatically for new users, they must be set manually

### Get Salary [/salary]

#### Get Employee Salary [GET]

+ Parameters
    + token (string) - The authentication token
    
+ Response 200 (application/json)
    + Body
        ```
        {
            "salary": "integer",
            "promotion_date": "datetime"
        }
        ```

## Models

### User

+ id
+ username (string, required) - The username of the user
+ password (string, required) - The password of the user

### Salary
+ id
+ salary (integer): The salary amount.
+ promotion_date (datetime): The date of the last promotion.
+ owner_id (integer): The id of the user associated with the salary record.

### Token

+ id (integer, required) - The unique identifier for the token
+ token (string, required) - The authentication token
+ owner_id (integer, required) - the od of the owner associated with the users table id column

## Dependencies

- Python 3.10
- Docker 26.1