from fastapi import Depends, APIRouter
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session

from sql_app import schemas
from sql_app import crud

from utils.utils import get_db
from utils.validators import usernameValidator, passwordValidator


router = APIRouter()


@router.post('/auth/register')
async def register(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_username(db, username=user.username)
    if db_user:
        return JSONResponse(content={"detail": "User with this username already exists"}, status_code=400)
    
    isValid, detail = usernameValidator(user.username)
    if not isValid:
        return JSONResponse(content={"detail": detail}, status_code=400)
    
    isValid, detail = passwordValidator(user.password)
    if not isValid:
        return JSONResponse(content={"detail": detail}, status_code=400)
    
    if user.username == user.password:
        return JSONResponse(content={"detail": "Username shouldn't be equal to password"}, status_code=400)
    
    db_user = crud.register(db, user)
    
    return JSONResponse(content={"detail": "Successfully registered. Login to get token."}, status_code=200)   


@router.post('/auth/login')
async def login(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = crud.login(db, user)
    if not db_user:
        return JSONResponse(content={"detail": "Invalid username or password"}, status_code=400)
    token_data = crud.create_user_token(db, db_user.id)

    response = {
        "token": token_data.token,
        "expiry": str(token_data.expiry)
    }

    return JSONResponse(content=response, status_code=200)
