from datetime import datetime, timezone
from fastapi import Depends, APIRouter
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session

from sql_app import crud

from utils.utils import get_db


router = APIRouter()


@router.get('/salary')
async def get_salary(token: str, db: Session = Depends(get_db)):
    if not token:
        return JSONResponse(content={"detail": "Token is missing"}, status_code=400)
    
    if len(token) > 64:
        return JSONResponse(content={"detail": "Invalid token length"}, status_code=400)
    
    token = crud.get_token(db, token)
    if not token:
        return JSONResponse(content={"detail": "Token is invalid"}, status_code=401)
    current_datetime = int(datetime.now(timezone.utc).timestamp())
    token_datetime = int(token.expiry.timestamp())
    if current_datetime > token_datetime:
        return JSONResponse(content={"detail": "Token has expired"}, status_code=400)
    
    salary = crud.get_salary(db, token.owner_id)
    if not salary:
        return JSONResponse(content={"detail": "Salary info not found or not set"}, status_code=200)
    response = {
        "salary": salary.salary,
        "promotion_date": salary.promotion_date.strftime("%Y-%m-%d %H:%M:%S")
    }
    
    return JSONResponse(content=response, status_code=200)
