from os import getenv
from dotenv import load_dotenv


load_dotenv()

HOST = getenv('HOST') or "127.0.0.1"
PORT = getenv('PORT') or "8000"
DEBUG = getenv('DEBUG') or "True"

if DEBUG != "True":
    POSTGRES_DB = getenv('POSTGRES_DB')
    POSTGRES_USER = getenv('POSTGRES_USER')
    POSTGRES_PASSWORD = getenv('POSTGRES_PASSWORD')
