from fastapi import FastAPI

from routers.auth import router as auth_router
from routers.salary import router as salary_router
from sql_app import models
from sql_app.database import engine

from config import HOST, PORT


app = FastAPI()
app.include_router(auth_router)
app.include_router(salary_router)

if __name__ == '__main__':
    models.Base.metadata.create_all(bind=engine)

    import uvicorn
    
    uvicorn.run(app, host=HOST, port=int(PORT))
