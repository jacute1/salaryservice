from sqlalchemy import text
import secrets
from bcrypt import hashpw, gensalt, checkpw

from sql_app.database import SessionLocal


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def generate_password(password: str):
    return hashpw(password.encode('utf-8'), gensalt())


def check_password(password: str, hashed_password: str):
    return checkpw(password.encode('utf-8'), hashed_password.encode('utf-8'))


def generate_token():
    return secrets.token_urlsafe(32)


def execute_sql_file(filepath: str):
    with open(filepath, 'r') as f:
        sql_commands = f.readlines()
    
    
    with SessionLocal() as session:
        for command in sql_commands:
            session.execute(text(command))
        session.commit()