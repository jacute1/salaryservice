import re


USERNAME_REGEXP = r'^[A-Za-z0-9]{8,}$'
PASSWORD_REGEXP = r'^[a-zA-Z0-9!#%&*+,-./:;<=>?_|~]{8,}$'


def usernameValidator(username: str):
    if len(username) < 8:
        return (False, "Username should be at least 8 characters")
    elif not re.match(USERNAME_REGEXP, username):
        return (False, "Username should contain only letters and numbers")
    return (True, None)


def passwordValidator(password: str):
    if len(password) < 8:
        return (False, "Password should be at least 8 characters")
    elif not re.match(PASSWORD_REGEXP, password):
        return (False, "Password should validate this regular expression '^[a-zA-Z0-9!#%&*+,-./:;<=>?_|~]{8,}$'")
    return (True, None)