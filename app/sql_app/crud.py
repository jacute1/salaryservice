import datetime
from sqlalchemy.orm import Session

from . import models, schemas
from utils.utils import generate_password, check_password, generate_token


def register(db: Session, user: schemas.UserCreate):
    hashed_password = generate_password(user.password).decode()
    db_user = models.User(username=user.username, password=hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def login(db: Session, user: schemas.UserCreate):
    db_user = get_user_by_username(db, user.username)
    if not db_user or not check_password(user.password, db_user.password):
        return None
    return db_user    


def get_user_by_username(db: Session, username: str):
    return db.query(models.User).filter(models.User.username == username).first()


def get_user_by_token(db: Session, token: str):
    return db.query(models.User).filter(models.User.tokens.contains(token)).first()


def create_user_token(db: Session, user_id: int):
    db_token = models.Token(token=generate_token(), owner_id=user_id)
    db.add(db_token)
    db.commit()
    db.refresh(db_token)
    return db_token


def get_token(db: Session, token: str):
    return db.query(models.Token).filter(models.Token.token == token).first()


def get_salary(db: Session, user_id: int):
    return db.query(models.Salary).filter(models.Salary.owner_id == user_id).first()


def create_salary(db: Session, owner_id: int, salary: int = None, promotion_date : datetime.datetime = None):
    db_salary = models.Salary(salary=salary, promotion_date=promotion_date, owner_id=owner_id)
    db.add(db_salary)
    db.commit()
    db.refresh(db_salary)
    
    return db_salary


def update_salary(db: Session, owner_id: int, salary: int, promotion_date : datetime.datetime):
    db_salary = db.query(models.Salary).filter(models.Salary.owner_id == owner_id).first()
    
    if db_salary:
        db_salary.salary = salary
        db_salary.promotion_date = promotion_date
        db.commit()
        return db_salary
    return None
