from sqlalchemy import Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.orm import relationship
from sql_app.database import Base

from datetime import datetime, timedelta, timezone


class User(Base):
    __tablename__ = "users"
    
    id = Column(Integer, primary_key=True)
    username = Column(String, unique=True, index=True)
    password = Column(String)
    
    tokens = relationship("Token", back_populates="owner")
    salaries = relationship("Salary", back_populates="owner")

class Token(Base):
    __tablename__ = "tokens"
    
    id = Column(Integer, primary_key=True)
    token = Column(String, unique=True, index=True)
    owner_id = Column(Integer, ForeignKey("users.id"))
    expiry = Column(DateTime, default=datetime.now(timezone.utc) + timedelta(hours=12))
    
    owner = relationship("User", back_populates="tokens")

class Salary(Base):
    __tablename__ = "salaries"
    
    id = Column(Integer, primary_key=True)
    salary = Column(Integer, nullable=True, default=None)
    promotion_date = Column(DateTime, nullable=True, default=None)
    owner_id = Column(Integer, ForeignKey("users.id"))
    
    owner = relationship("User", back_populates="salaries")
