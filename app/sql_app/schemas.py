from pydantic import BaseModel
from datetime import datetime


class TokenBase(BaseModel):
    token: str

class Token(TokenBase):
    id: int
    owner_id: int
    
    class Config:
        from_attributes = True


class SalaryBase(BaseModel):
    salary: str
    promotion_date: datetime

class Salary(SalaryBase):
    id: int
    owner_id: int
    
    class Config:
        from_attributes = True


class UserBase(BaseModel):
    username: str

class UserCreate(UserBase):
    password: str
    
class User(UserBase):
    id: int
    token: Token
    salary: Salary
    
    class Config:
        from_attributes = True
