FROM python:3.10-alpine

WORKDIR /SalaryService

COPY ./app .
COPY ./requirements.txt .

RUN pip3 install -r requirements.txt

CMD ["python3", "main.py"]